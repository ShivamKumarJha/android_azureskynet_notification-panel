package shivam.notificationpanel;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.but1)
        .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationCompat.Builder ncb = new NotificationCompat.Builder(MainActivity.this);
                ncb.setSmallIcon(R.drawable.ic_stat_sentiment_very_satisfied);
                ncb.setContentText("Notification panel demo.");
                ncb.setContentTitle(getResources().getString(R.string.app_name));
                ncb.setSubText("Notification subtext.");
                ncb.setTicker("Ticker");
                Intent intent = new Intent(MainActivity.this,Post.class); //im too lazy to create a new activity :D
                PendingIntent contentIntent = PendingIntent.getActivity(MainActivity.this, 0, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                ncb.setContentIntent(contentIntent);
                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.notify(0,ncb.build());
            }
        });
    }
}
